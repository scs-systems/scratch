# Example docker build command
TAG="1.0"
IMAGE="gitlab.scsuk.net:5005/scs-systems/scratch:$TAG"
docker build -t "$IMAGE" .
docker push "$IMAGE"
